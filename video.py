#! /usr/bin/python2
import cv
import cv2
import sys
import numpy
import random

if len(sys.argv) != 2:
	print("Usage: python video.py image")
	exit(1)

WIDTH = 640
HEIGHT = 480
FRAMES = 120
FPS = 60
VIDEO_FILE = "video.avi"
POSITION_FILE = "positions.txt"

MOVE_PERCENTAGE = 0.25 # Only allow MOVE_PERCENTAGE of the roi to change per frame

def cv2array(im):
  depth2dtype = {
        cv.IPL_DEPTH_8U: 'uint8',
        cv.IPL_DEPTH_8S: 'int8',
        cv.IPL_DEPTH_16U: 'uint16',
        cv.IPL_DEPTH_16S: 'int16',
        cv.IPL_DEPTH_32S: 'int32',
        cv.IPL_DEPTH_32F: 'float32',
        cv.IPL_DEPTH_64F: 'float64',
    }

  a = numpy.fromstring(
         im.tostring(),
         dtype=depth2dtype[im.depth],
         count=im.width*im.height*im.nChannels)
  a.shape = (im.height,im.width,im.nChannels)
  return a

image = cv.LoadImage(sys.argv[1])
if ((WIDTH * (1 + MOVE_PERCENTAGE)) >  image.width) or \
   (HEIGHT * (1 + MOVE_PERCENTAGE)) > image.height:
	print("Image is too small")
	exit(1)


video_out = cv2.VideoWriter()
assert(True == video_out.open(VIDEO_FILE,
								   cv.CV_FOURCC('M', 'J', 'P', 'G'),
								   FPS,
								   (WIDTH, HEIGHT)))
assert(True == video_out.isOpened())

with open(POSITION_FILE, "w+") as posFile:
	pos = (0, 0)
	for i in range(0, FRAMES):
		#video_out.write(cv2array(cv.GetSubRect(image, (pos[0], pos[1], WIDTH, HEIGHT))))
		video_out.write(numpy.asarray(cv.GetSubRect(image, (pos[0], pos[1], WIDTH, HEIGHT))))

		xmin = max(0, pos[0] - MOVE_PERCENTAGE * WIDTH)
		xmax = min(image.width - WIDTH, pos[0] + MOVE_PERCENTAGE * WIDTH)
		ymin = max(0, pos[1] - MOVE_PERCENTAGE * HEIGHT)
		ymax = min(image.height - HEIGHT, pos[1] + MOVE_PERCENTAGE * HEIGHT)
		x = random.randint(xmin, xmax)
		y = random.randint(ymin, ymax)
		posFile.write("%d %d\n" % (x - pos[0], y - pos[1]))
		pos = (x, y)
	video_out.release()
