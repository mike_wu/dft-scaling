CXX			= g++
CFLAGS		= -Wall -g
CXXFLAGS	+= `pkg-config opencv --cflags`
LDFLAGS		+= `pkg-config opencv --libs` -lrt

all			= phase
all: $(all)

clean:
	rm -rf $(all) *.o
