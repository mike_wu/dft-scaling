#! /usr/bin/python2
import sys
import matplotlib.pyplot as pyplot

if len(sys.argv) != 2:
	print("Usage: python display_results.py results.txt")
	sys.exit(1)
scale = []
time = []
error = []
# Square root and normalize error by 120 frames
with open(sys.argv[1]) as resultFile:
	for line in resultFile.readlines():
		line = line.strip().split()
		scale.append(line[0])
		time.append(line[1])
		error.append(float(line[2])**0.5 / 120)

pyplot.plot(scale, time)
pyplot.title("Scale vs time")

pyplot.figure()
pyplot.plot(scale, error)
pyplot.title("Scale vs error")

pyplot.figure()
pyplot.plot(time, error)
pyplot.title("time vs error")
pyplot.show()
