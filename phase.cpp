#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <time.h>
#include <stdio.h>
#include <vector>
using namespace cv;

#define NANOSECONDS_PER_SECOND 1000000000LL


long long int time() {
	struct timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);
	return ts.tv_sec * NANOSECONDS_PER_SECOND + ts.tv_nsec;
}

int ShowImage(Mat frame) {
	imshow("video", frame);
	return waitKey(27);
}

// Compute the L2 norm to compute error
float ComputeError(vector<Point2d> move, const char *pos_filename) {
	FILE * pos_file = fopen(pos_filename, "r");
	if(pos_file == NULL) {
		fprintf(stderr, "Could not open position file\n");
		exit(EXIT_FAILURE);
	}
	float norm = 0.0;
	float x, y;
	Point2d p;
	int num_points = 0;
	vector<Point2d>::iterator it = move.begin();

	while(fscanf(pos_file, "%f %f\n", &x, &y) == 2) {
		p = *it;
		// For some reason, all movement are backwards, w/e
		float diff = (-1*p.x - x) * (-1*p.x - x) + (-1*p.y - y) * (-1*p.y - y);
		//printf("%f (%f %f) (%f %f)\n", diff, x, y, p.x, p.y);
		norm += diff;
		num_points++;

		it++;
		if(it == move.end()) {
			break;
		}
	}

	if(move.size() != num_points) {
		fprintf(stderr,"Number of movements in positions file and video did not match\n");
		exit(EXIT_FAILURE);
	}
	fclose(pos_file);
	return norm;
}

void RunTest(float scale, const char *video_file, const char *pos_filename) {
	VideoCapture video(video_file);
	long long int start_time, run_time;
	float accuracy = 0.0;

	start_time = time();
	Mat prev, curr, hanning;

	video >> prev;
	resize(prev, prev, Size(0,0), scale, scale);
	cvtColor(prev, prev, CV_RGB2GRAY);
	prev.convertTo(prev, CV_32F);
	video >> curr;
	resize(curr, curr, Size(0,0), scale, scale);
	cvtColor(curr, curr, CV_RGB2GRAY);
	curr.convertTo(curr, CV_32F);

	createHanningWindow(hanning, prev.size(), CV_32F);
	vector<Point2d> move;

	while(!curr.empty()) {
		Point2d point = phaseCorrelate(prev, curr, hanning);
		point.x = point.x / scale;
		point.y = point.y / scale;
		move.push_back(point);

		prev = curr.clone();
		video >> curr;
		if(curr.empty()) {
			break;
		}
		resize(curr, curr, Size(0,0), scale, scale);
		cvtColor(curr, curr, CV_RGB2GRAY);
		curr.convertTo(curr, CV_32F);
	}
	run_time = time() - start_time;
	accuracy = ComputeError(move, pos_filename);
	printf("%f %f %f\n", scale, float(run_time) / NANOSECONDS_PER_SECOND, accuracy);
}

int main(int argc, char **argv) {

	if(argc != 3) {
		fprintf(stderr, "Usage: phase video.avi position.txt\n");
		exit(EXIT_FAILURE);
	}

	for(float scale = 0.05; scale < 1.0; scale += 0.05) {
		RunTest(scale, argv[1], argv[2]);
	}
	// Floating point errors accumulate, doesn't seem to like doing the baseline
	RunTest(1, argv[1], argv[2]);

	return 0;
}

